/*
Michel Martinez - 2016

Código que limpa as configurações da ESP8266, é util para quando a mesma apresenta algum defeito,
Já me auxiliou algumas vezes quando haviam problemas na memória Flash, ou em alguma configuração
do Wifi.
*/



#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266httpUpdate.h>

void setup() {
  Serial.begin(9600);
  WiFi.softAPdisconnect(true);
  WiFi.disconnect(true);
  delay(100);
  ESP.eraseConfig();
  Serial.println("Vai reiniciar, depois disto grave seu programa")
  ESP.reset();
}

void loop() {
  Serial.println("O processo deu errado, tente novamente");
  Serial.println("Regrave!");  
}
